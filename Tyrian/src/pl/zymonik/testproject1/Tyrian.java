package pl.zymonik.testproject1;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;

public class Tyrian implements ApplicationListener {

	public static final String TAG = Tyrian.class.getSimpleName();
	private FPSLogger fpsLogger;
	
	@Override
	public void create() {
		
		Gdx.app.log(TAG, "Creating game");
		fpsLogger = new FPSLogger();

	}

	@Override
	public void dispose() {
		Gdx.app.log( Tyrian.TAG, "Disposing game" );
	}

	@Override
	public void render() {		
        // the following code clears the screen with the given RGB color (green)
        Gdx.gl.glClearColor( 0f, 1f, 0f, 1f );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

        // output the current FPS
        fpsLogger.log();
	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log(Tyrian.TAG, "Resizing game to: " + width + " x " + height );
	}

	@Override
	public void pause() {
		Gdx.app.log(Tyrian.TAG, "Pausing game" );
	}

	@Override
	public void resume() {
		Gdx.app.log( Tyrian.TAG, "Resuming game" );
	}
}
